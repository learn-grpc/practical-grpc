#!/usr/bin/env bash

# first install the protoc-gen-go plugin
# go get github.com/golang/protobuf/protoc-gen-go
protoc proto/sfapi.proto --go_out=plugins=grpc:.
